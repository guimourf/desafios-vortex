//Questao 1:
function questao1(){
    let romanos = [];
    let resultado = 0;
    let aux;

    aux = document.getElementById("romano").value;

    romanos = aux.split("");
    console.log(romanos);

    for(let i = 1; i < romanos.length; i++){
        if(romanos[i] == "V" && romanos[i-1] == "I"){
            resultado -= 2;
        }
        if(romanos[i] == "X" && romanos[i-1] == "I"){
            resultado -= 2;
        }
        if(romanos[i] == "L" && romanos[i-1] == "X"){
            resultado -= 20;
        }
        if(romanos[i] == "C" && romanos[i-1] == "X"){
            resultado -= 20;
        }
        if(romanos[i] == "D" && romanos[i-1] == "C"){
            resultado -= 200;
        }
        if(romanos[i] == "M" && romanos[i-1] == "C"){
            resultado -= 200;
        }
    }

    console.log(resultado)

    for(let i = 0; i < romanos.length; i++){
        if(romanos[i] == "I"){
            resultado += 1;
        } else if(romanos[i] == "V"){
            resultado += 5;
        } else if(romanos[i] == "X"){
            resultado += 10;
        } else if(romanos[i] == "L"){
            resultado += 50;
        } else if(romanos[i] == "C"){
            resultado += 100;
        } else if(romanos[i] == "D"){
            resultado += 500;
        } else if(romanos[i] == "M"){
            resultado += 1000;
        }
    }

    document.getElementById("resultado").innerHTML = resultado;
    console.log(resultado)

}